package pit.ggtime;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.appindexing.FirebaseAppIndex;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.appindexing.Action;
import com.google.firebase.appindexing.FirebaseUserActions;
import com.google.firebase.appindexing.Indexable;
import com.google.firebase.appindexing.builders.Actions;

import pit.ggtime.loginFacebook.FirstTimeLoginFacebook;
import pit.ggtime.insideApp.MainUserView;

public class MainActivity extends AppCompatActivity{
    private CallbackManager callbackManager;
    private FirebaseDatabase firebaseDatabase;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private LoginButton loginButton;
    private Indexable user;

    private LinearLayout main_activity_Linear;
    private ProgressBar progressBar;

    private FacebookCallback<LoginResult> mCallback=new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            main_activity_Linear.setVisibility(View.INVISIBLE);

            progressBar.setVisibility(View.VISIBLE);
            progressBar.bringToFront();

            handleFacebookAccessToken(loginResult.getAccessToken());
        }
        @Override
        public void onCancel() {
            Toast.makeText(MainActivity.this, "Sign In canceled", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onError(FacebookException error) {
            Toast.makeText(MainActivity.this, "Something bad happened", Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();

        //AuthStateListener is an Object that "listens" to any change on the Authentication state.
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(final FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    //Check node existence :DDDD
                    //Get a reference to the user id node on the Database
                    DatabaseReference userRef = firebaseDatabase.getReference().child("users").child(firebaseAuth.getCurrentUser().getUid());
                    //Add a listener to the reference.
                    userRef.addListenerForSingleValueEvent(new ValueEventListener() {
                        //When data changes, this code is executed. But it's executed on the first run as well.
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            //If the User id exists, proceed to the MainUserView
                            if(dataSnapshot.exists() == true){
                                goToMainUserView();
                            }
                            //If not, register it on the database
                            else{
                                firebaseDatabase.getReference().child("users").child(firebaseAuth.getCurrentUser().getUid()).child("facebook_id").setValue(Profile.getCurrentProfile().getId());
                                firebaseDatabase.getReference().child("users").child(firebaseAuth.getCurrentUser().getUid()).child("nome").setValue(Profile.getCurrentProfile().getName());
                                //Go to the Activity which gets the user data.
                                goToFirstLoginFacebook();
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                    Log.d("TG", "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    Log.d("TG", "SIGNED OUT");
                }
            }
        };

        callbackManager = CallbackManager.Factory.create();
        loginButton = findViewById(R.id.login_button);

        progressBar = (ProgressBar) findViewById(R.id.main_activity_progressbar);

        main_activity_Linear = (LinearLayout) findViewById(R.id.main_activity_Linear);

        //Set the permissions asked to facebook user
        loginButton.setReadPermissions("email", "public_profile", "user_friends");

        //Register the previously created CallBackManager, and CallBack
        loginButton.registerCallback(callbackManager, mCallback);
    }

    public void goToFirstLoginFacebook() {
        Intent intent = new Intent(this, FirstTimeLoginFacebook.class);
        //intent.
        startActivity(intent);
    }

    /*public void goToWelcomeActivity(){
        Intent intent = new Intent(this, WelcomeScreen.class);
        //intent.
        startActivity(intent);
    }*/

    public void goToMainUserView(){
        Intent intent = new Intent(this, MainUserView.class);
        //intent.
        startActivity(intent);
    }

    //OnStart of the activity
    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);

        //FirebaseAppIndex.getInstance().update(getIndexable());
        //FirebaseUserActions.getInstance().start(getAction());
    }

    //When the activity is stopped
    @Override
    public void onStop() {
        //FirebaseUserActions.getInstance().end(getAction());
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    private void handleFacebookAccessToken(AccessToken token) {
        Log.d("TG", "handleFacebookAccessToken:" + token);

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d("TG", "signInWithCredential:onComplete:" + task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Log.w("TG", "signInWithCredential", task.getException());
                            Toast.makeText(MainActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }

                        // ...
                    }
                });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode,
                resultCode, data);
    }

    /*public Action getAction() {
        return Actions.newView("http://host/path", "android-app://pit.ggtime/http/host/path");
    }*/


    /*public void onClick(View v) {
        mAuth.signOut();
        LoginManager.getInstance().logOut();
    }*/
}
