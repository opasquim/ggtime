package pit.ggtime.loginFacebook;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.sql.Ref;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import pit.ggtime.R;
import pit.ggtime.insideApp.MainUserView;
import pit.ggtime.utis.User;

public class Preferences extends AppCompatActivity {

    private Spinner spinner1;
    private Spinner spinner2;
    private Spinner spinner3;
    private Spinner spinner4;
    private Spinner spinner5;;

    private static final String[] path1 = {"Bronze", "Prata", "Ouro", "Platina", "Diamante", "Mestre", "Desafiante"};
    private static final String[] path2 = {"1", "2", "3", "4", "5"};
    private static final String[] path3 = {"Top", "Jg", "Mid", "Adc", "Suporte"};

    private Button continueButton;

    private ArrayAdapter adapter1;
    private ArrayAdapter adapter2;
    private ArrayAdapter adapter3;
    private ArrayAdapter adapter4;
    private ArrayAdapter adapter5;

    private FirebaseUser user;
    private FirebaseDatabase firebaseDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preferences);

        user = FirebaseAuth.getInstance().getCurrentUser();
        firebaseDatabase = FirebaseDatabase.getInstance();

        //Button
        continueButton = (Button) findViewById(R.id.first_time_login_button);

        spinner1 = (Spinner)findViewById(R.id.spinner_elo);
        adapter1 = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,path1);

        spinner2 = (Spinner)findViewById(R.id.spinner_level);
        adapter2 = new ArrayAdapter(this,android.R.layout.simple_spinner_item,path2);

        spinner3 = (Spinner)findViewById(R.id.spinner_elo2);
        adapter3 = new ArrayAdapter(this,android.R.layout.simple_spinner_item,path1);

        spinner4 = (Spinner)findViewById(R.id.spinner_level2);
        adapter4 = new ArrayAdapter(this,android.R.layout.simple_spinner_item,path2);

        spinner5 = (Spinner)findViewById(R.id.spinner_lane);
        adapter5 = new ArrayAdapter(this,android.R.layout.simple_spinner_item,path3);

        spinner1.setAdapter(adapter1);
        spinner2.setAdapter(adapter2);
        spinner3.setAdapter(adapter3);
        spinner4.setAdapter(adapter4);
        spinner5.setAdapter(adapter5);

        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mmin;
                String mminlevel;
                String mmax;
                String mmaxlevel;
                String mlane;


                    mmin = spinner1.getSelectedItem().toString();
                    mminlevel = spinner2.getSelectedItem().toString();
                    mmax = spinner3.getSelectedItem().toString();
                    mmaxlevel = spinner4.getSelectedItem().toString();
                    mlane = spinner5.getSelectedItem().toString();

                    firebaseDatabase.getReference().child("users").child(user.getUid()).child("min").setValue(mmin);
                    firebaseDatabase.getReference().child("users").child(user.getUid()).child("minlevel").setValue(mminlevel);
                    firebaseDatabase.getReference().child("users").child(user.getUid()).child("max").setValue(mmax);
                    firebaseDatabase.getReference().child("users").child(user.getUid()).child("maxlevel").setValue(mmaxlevel);
                    firebaseDatabase.getReference().child("users").child(user.getUid()).child("duolane").setValue(mlane);


                goToMainUserView();

            }
        });
    }

    public void goToMainUserView(){
        Intent intent = new Intent(this, MainUserView.class);
        //intent.
        startActivity(intent);
    }
}