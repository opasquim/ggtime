package pit.ggtime.loginFacebook;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.sql.Ref;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import pit.ggtime.R;
import pit.ggtime.loginFacebook.Preferences;
import pit.ggtime.utis.User;

public class FirstTimeLoginFacebook extends AppCompatActivity {

    private EditText user_name;

    private Spinner spinner1;
    private Spinner spinner2;
    private Spinner spinner3;

    private static final String[] path1 = {"Bronze", "Prata", "Ouro", "Platina", "Diamante", "Mestre", "Desafiante"};
    private static final String[] path2 = {"1", "2", "3", "4", "5"};
    private static final String[] path3 = {"Top", "Jg", "Mid", "Adc", "Suporte"};

    private Button continueButton;

    private ArrayAdapter adapter1;
    private ArrayAdapter adapter2;
    private ArrayAdapter adapter3;

    private FirebaseUser user;
    private FirebaseDatabase firebaseDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_time_login_facebook);

        user = FirebaseAuth.getInstance().getCurrentUser();
        firebaseDatabase = FirebaseDatabase.getInstance();

        //Editboxes
        user_name = (EditText) findViewById(R.id.first_time_summoner_text);

        //Button
        continueButton = (Button) findViewById(R.id.first_time_login_button);

        spinner1 = (Spinner)findViewById(R.id.spinner_elo);
        adapter1 = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,path1);

        spinner2 = (Spinner)findViewById(R.id.spinner_level);
        adapter2 = new ArrayAdapter(this,android.R.layout.simple_spinner_item,path2);

        spinner3 = (Spinner)findViewById(R.id.spinner_lane);
        adapter3 = new ArrayAdapter(this,android.R.layout.simple_spinner_item,path3);

        spinner1.setAdapter(adapter1);
        spinner2.setAdapter(adapter2);
        spinner3.setAdapter(adapter3);

        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name;
                String myelo;
                String mylevel;
                String mylane;


                if (user_name.getText() == null) {

                } else{
                    name = user_name.getText().toString();
                    myelo = spinner1.getSelectedItem().toString();
                    mylevel = spinner2.getSelectedItem().toString();
                    mylane = spinner3.getSelectedItem().toString();

                    firebaseDatabase.getReference().child("users").child(user.getUid()).child("username").setValue(name);
                    firebaseDatabase.getReference().child("users").child(user.getUid()).child("elo").setValue(myelo);
                    firebaseDatabase.getReference().child("users").child(user.getUid()).child("divisao").setValue(mylevel);
                    firebaseDatabase.getReference().child("users").child(user.getUid()).child("lane").setValue(mylane);

                    goToPreferences();

                    }



                }
            });
    }

    public void goToPreferences(){
        Intent intent = new Intent(this, Preferences.class);
        //intent.
        startActivity(intent);
    }
}
