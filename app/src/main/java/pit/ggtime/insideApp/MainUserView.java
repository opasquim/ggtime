package pit.ggtime.insideApp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import pit.ggtime.R;


public class MainUserView extends AppCompatActivity {
    private ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main_user_view);

        progressBar = (ProgressBar) findViewById(R.id.main_user_progressbar);
        progressBar.setVisibility(View.VISIBLE);
        progressBar.bringToFront();
    }
}
